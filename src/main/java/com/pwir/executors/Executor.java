package com.pwir.executors;

interface Executor {

    void execute();
}
