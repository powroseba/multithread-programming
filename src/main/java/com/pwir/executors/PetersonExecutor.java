package com.pwir.executors;

import java.util.concurrent.ExecutorService;

class PetersonExecutor implements Executor {

    private final ExecutorService executorService;
    private final Runnable firstPetersonInstance;
    private final Runnable secondPetersonInstance;

    PetersonExecutor(ExecutorService executorService, Runnable firstPetersonInstance, Runnable secondPetersonInstance) {
        this.executorService = executorService;
        this.firstPetersonInstance = firstPetersonInstance;
        this.secondPetersonInstance = secondPetersonInstance;
    }

    @Override
    public void execute() {
        runPetersonThreads();
    }

    private void runPetersonThreads() {
        executorService.execute(firstPetersonInstance);
        executorService.execute(secondPetersonInstance);
    }
}
