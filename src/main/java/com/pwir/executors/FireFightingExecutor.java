package com.pwir.executors;

import java.util.concurrent.ExecutorService;
import java.util.stream.Stream;

class FireFightingExecutor implements Executor {

    private final ExecutorService executorService;
    private final Runnable[] firefighters;

    FireFightingExecutor(ExecutorService executorService, Runnable[] firefighters) {
        this.executorService = executorService;
        this.firefighters = firefighters;
    }

    @Override
    public void execute() {
        Stream.of(firefighters).forEach(executorService::execute);
    }
}
