package com.pwir.executors;

import com.pwir.config.SystemProperties;

import java.util.concurrent.ExecutorService;

class ProducerConsumerExecutor implements Executor {

    private static final String PRODUCER_AMOUNT_PROPERTY = "producer.amount";
    private static final String CONSUMER_AMOUNT_PROPERTY = "consumer.amount";

    private final ExecutorService executorService;
    private final Runnable producer;
    private final Runnable consumer;

    ProducerConsumerExecutor(ExecutorService executorService, Runnable producer, Runnable consumer) {
        this.executorService = executorService;
        this.producer = producer;
        this.consumer = consumer;
    }

    @Override
    public void execute() {
        final int objectProduced = SystemProperties.getPropertyAsInteger(PRODUCER_AMOUNT_PROPERTY);
        final int objectConsumed = SystemProperties.getPropertyAsInteger(CONSUMER_AMOUNT_PROPERTY);

        for (int i = 0; i < objectProduced; i++) {
            executorService.execute(producer);
        }
        for (int i = 0; i < objectConsumed; i++) {
            executorService.execute(consumer);
        }

    }
}
