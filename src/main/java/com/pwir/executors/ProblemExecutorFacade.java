package com.pwir.executors;

import com.pwir.config.SystemProperties;
import com.pwir.problems.ProblemFactoryFacade;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

public class ProblemExecutorFacade {

    private static final String QUEUE_SIZE_PROPERTY = "producer.consumer.queue.size";

    private final ExecutorService threadPool;
    private final ProblemFactoryFacade problemFactoryFacade;

    public ProblemExecutorFacade(ExecutorService threadPool, ProblemFactoryFacade problemFactoryFacade) {
        this.threadPool = threadPool;
        this.problemFactoryFacade = problemFactoryFacade;
    }

    public void executePetersonProblem() {
        System.out.println("Executing Peterson Problem for 2 threads...\n\n");
        new PetersonExecutor(
                threadPool,
                problemFactoryFacade.buildPeterson(0),
                problemFactoryFacade.buildPeterson(1)
        ).execute();
    }

    public void executeProducerConsumerProblem() {
        System.out.println("Executing Producer-Consumer Problem on 7 threads ...\n\n");
        BlockingQueue<String> queue = new LinkedBlockingQueue<>(SystemProperties.getPropertyAsInteger(QUEUE_SIZE_PROPERTY));
        new ProducerConsumerExecutor(
                threadPool,
                problemFactoryFacade.buildProducer(queue),
                problemFactoryFacade.buildConsumer(queue)
        ).execute();
    }

    public void executeFireFightingProblem() {
        System.out.println("Executing fire fighting problem on 3 threads");
        new FireFightingExecutor(
                threadPool, problemFactoryFacade.buildFirefighters()
        ).execute();
    }

    public void executeBusStationProblem() {
        System.out.println("Executing bus station problem on 4 threads");
        String busStopName = "MultiThreadBusStation";
        Runnable[] passengers = new Runnable[] {
            problemFactoryFacade.buildPassenger(busStopName),
            problemFactoryFacade.buildPassenger(busStopName),
            problemFactoryFacade.buildPassenger(busStopName)
        };
        new BusStationExecutor(
                threadPool, problemFactoryFacade.buildBus(busStopName), passengers
        ).execute();
    }
}
