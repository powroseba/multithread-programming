package com.pwir.executors;

import java.util.concurrent.ExecutorService;

class BusStationExecutor implements Executor {

    private final ExecutorService executorService;
    private final Runnable busInstance;
    private final Runnable[] passengerInstances;

    BusStationExecutor(ExecutorService executorService, Runnable busInstance, Runnable... passengerInstances) {
        this.executorService = executorService;
        this.busInstance = busInstance;
        this.passengerInstances = passengerInstances;
    }


    @Override
    public void execute() {
        executorService.execute(busInstance);
        for (int i = 0; i < passengerInstances.length; i++) {
            executorService.execute(passengerInstances[i]);
        }
    }
}
