package com.pwir.problems.station;

class Passenger implements Runnable {

    private final String busStop;

    Passenger(String busStop) {
        this.busStop = busStop;
    }

    @Override
    public void run() {
        System.out.println("Passenger " + Thread.currentThread().getId() + " is walking...");
        waiting(2);

        synchronized (busStop) {
            try {
                System.out.println("Passenger " + Thread.currentThread().getId() + " is waiting for a bus...");
                busStop.wait();
            } catch (InterruptedException e) {
                System.out.println("Passenger " + Thread.currentThread().getId() + " waiting was interrupted!");
            }
        }
        System.out.println("Passenger " + Thread.currentThread().getId() + " is getting on the bus");
    }

    private void waiting(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            System.err.println("Bus ride was interrupted");
        }
    }
}
