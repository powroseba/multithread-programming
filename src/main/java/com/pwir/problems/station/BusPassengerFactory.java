package com.pwir.problems.station;

public class BusPassengerFactory {

    public Runnable buildBus(String busName) {
        return new Bus(busName);
    }

    public Runnable buildPassenger(String busName) {
        return new Passenger(busName);
    }
}
