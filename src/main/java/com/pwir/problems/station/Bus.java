package com.pwir.problems.station;

class Bus implements Runnable {

    private final String busStop;

    Bus(String busStop) {
        this.busStop = busStop;
    }

    @Override
    public void run() {
        System.out.println("Bus is riding ...");
        waiting(4);
        synchronized (busStop) {
            System.out.println("Bus is waiting...");
            waiting(2);
            busStop.notifyAll();
        }
        waiting(2);
        System.out.println("Bus is leaving the station...");
        waiting(1);
    }

    private void waiting(long seconds) {
        try {
            Thread.sleep(seconds * 1000);
        } catch (InterruptedException e) {
            System.err.println("Bus ride was interrupted");
        }
    }
}
