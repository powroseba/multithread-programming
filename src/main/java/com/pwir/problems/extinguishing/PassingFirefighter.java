package com.pwir.problems.extinguishing;

import java.util.concurrent.Exchanger;

class PassingFirefighter extends AbstractFirefighter {

    private final Exchanger<Bucket> fillBucketExchanger;
    private final Exchanger<Bucket> extinguishFireBucketExchanger;

    PassingFirefighter(int id, Fire fire, Exchanger<Bucket> fillBucketExchanger, Exchanger<Bucket> extinguishFireBucketExchanger) {
        super(id, fire);
        this.fillBucketExchanger = fillBucketExchanger;
        this.extinguishFireBucketExchanger = extinguishFireBucketExchanger;
    }

    @Override
    public void work() throws InterruptedException {
        if (ownBucket.isFull()) {
            System.out.println("PassingFirefighter_" + id + " : passing filled bucket to right ...");
            this.ownBucket = extinguishFireBucketExchanger.exchange(ownBucket);
        } else {
            System.out.println("PassingFirefighter_" + id + " : passing empty bucket to left ...");
            this.ownBucket = fillBucketExchanger.exchange(ownBucket);
        }
    }
}
