package com.pwir.problems.extinguishing;

interface Firefighter extends Runnable {

    void work() throws InterruptedException;

    class Bucket {
        private final int id;
        private boolean full;

        Bucket(int id, boolean full) {
            this.id = id;
            this.full = full;
        }

        boolean isFull() {
            return full;
        }

        void setFull(boolean full) {
            this.full = full;
        }
    }
}
