package com.pwir.problems.extinguishing;

import java.util.concurrent.Exchanger;

class ExtinguishingFirefighter extends AbstractFirefighter {

    private final Exchanger<Bucket> extinguishFireBucketExchanger;

    ExtinguishingFirefighter(int id, Fire fire, Exchanger<Bucket> extinguishFireBucketExchanger) {
        super(id, fire);
        this.extinguishFireBucketExchanger = extinguishFireBucketExchanger;
    }

    @Override
    public void work() throws InterruptedException {
        if (ownBucket.isFull()) {
            System.out.println("ExtinguishingFirefighter_" + id + " : extinguishing...");
            fire.extinguish();
            ownBucket.setFull(false);
        } else {
            System.out.println("ExtinguishingFirefighter_" + id + " : passing empty bucket to left ...");
            this.ownBucket = extinguishFireBucketExchanger.exchange(ownBucket);
        }
    }
}
