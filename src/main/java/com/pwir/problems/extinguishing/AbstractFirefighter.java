package com.pwir.problems.extinguishing;

abstract class AbstractFirefighter implements Firefighter {

    protected final int id;
    protected final Fire fire;
    protected Bucket ownBucket;

    AbstractFirefighter(int id, Fire fire) {
        this.id = id;
        this.fire = fire;
        this.ownBucket = new Bucket(id, false);
    }

    @Override
    public void run() {
        try {
            do {
                work();
                sleep();
            } while (this.fire.isDanger());
        } catch (InterruptedException ex) {
            System.out.println("WEYOO WEYOO extinguishing of fire interrupted!!!");
        }
    }

    private void sleep() throws InterruptedException {
        Thread.sleep(250);
    }
}
