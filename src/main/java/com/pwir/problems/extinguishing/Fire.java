package com.pwir.problems.extinguishing;

class Fire {

    private volatile int fireLevel;

    Fire(int fireLevel) {
        this.fireLevel = fireLevel;
    }

    boolean isDanger() {
        return fireLevel >= 1;
    }

    void extinguish() {
        this.fireLevel = fireLevel - 1;
    }

}
