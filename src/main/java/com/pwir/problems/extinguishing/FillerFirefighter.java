package com.pwir.problems.extinguishing;

import java.util.concurrent.Exchanger;

class FillerFirefighter extends AbstractFirefighter {

    private final Exchanger<Bucket> fillBucketExchanger;

    FillerFirefighter(int id, Fire fire, Exchanger<Bucket> fillBucketExchanger) {
        super(id, fire);
        this.fillBucketExchanger = fillBucketExchanger;
    }

    @Override
    public void work() throws InterruptedException {
        if (ownBucket.isFull()) {
            System.out.println("FillerFirefighter_" + id + " : passing filled bucket to right...");
            this.ownBucket = fillBucketExchanger.exchange(ownBucket);
        } else {
            System.out.println("FillerFirefighter_" + id + " : filling...");
            ownBucket.setFull(true);
        }
    }
}
