package com.pwir.problems.extinguishing;

import com.pwir.config.SystemProperties;

import java.util.concurrent.Exchanger;

public class FirefightersFactory {

    private static final String FIRE_LEVEL_PROPERTY = "fire.level";
    private static final String AMOUNT_OF_FIREFIGHTERS = "firefighters.amount";

    public Runnable[] buildFirefighters() {
        Firefighter[] firefighters = new Firefighter[SystemProperties.getPropertyAsInteger(AMOUNT_OF_FIREFIGHTERS)];
        Fire fire = new Fire(SystemProperties.getPropertyAsInteger(FIRE_LEVEL_PROPERTY));

        Exchanger<Firefighter.Bucket> firstExchange = new Exchanger<>();
        Exchanger<Firefighter.Bucket> secondExchange = new Exchanger<>();
        for (int i = 0; i < firefighters.length; i++) {
            if (i == 0) {
                firefighters[i] = new FillerFirefighter(i, fire, firstExchange);
            }
            else if (i == firefighters.length - 1) {
                firefighters[i] = new ExtinguishingFirefighter(i, fire, firstExchange);
            } else {
                firefighters[i] = new PassingFirefighter(i ,fire, firstExchange, secondExchange);
                firstExchange = secondExchange;
                secondExchange = new Exchanger<>();

            }
        }
        return firefighters;
    }
}
