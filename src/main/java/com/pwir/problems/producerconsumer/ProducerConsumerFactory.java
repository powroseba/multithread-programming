package com.pwir.problems.producerconsumer;

import com.pwir.config.SystemProperties;
import com.pwir.generators.ResourceGenerator;
import com.pwir.generators.StringResourceGenerator;

import java.util.concurrent.BlockingQueue;

public class ProducerConsumerFactory {

    private static final String PRODUCER_CONSUMER_EXECUTION_TIME_PROPERTY = "producer.consumer.execution.time_in_second";

    public Runnable buildProducer(BlockingQueue<String> queue) {
        ResourceGenerator<String> generator = new StringResourceGenerator();
        return new Producer<>(queue, generator, SystemProperties.getPropertyAsInteger(PRODUCER_CONSUMER_EXECUTION_TIME_PROPERTY));
    }

    public Runnable buildConsumer(BlockingQueue<String> queue) {
        return new Consumer<>(queue, SystemProperties.getPropertyAsInteger(PRODUCER_CONSUMER_EXECUTION_TIME_PROPERTY));
    }
}
