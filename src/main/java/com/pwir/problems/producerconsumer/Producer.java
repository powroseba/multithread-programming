package com.pwir.problems.producerconsumer;

import com.pwir.generators.ResourceGenerator;

import java.util.concurrent.BlockingQueue;

class Producer<T> implements Runnable {

    private final BlockingQueue<T> queue;
    private final ResourceGenerator<T> generator;
    private final long workTimeInSeconds;
    private final long startTime;

    Producer(BlockingQueue<T> queue, ResourceGenerator<T> generator, long workTimeInSeconds) {
        this.queue = queue;
        this.generator = generator;
        this.workTimeInSeconds = workTimeInSeconds * 1000;
        this.startTime = System.currentTimeMillis();
    }

    @Override
    public void run() {
        try {
            produce();
        } catch (InterruptedException ex) {
            System.err.println("Producer INTERRUPTED!");
        }
    }

    private void produce() throws InterruptedException {
        while (System.currentTimeMillis() - startTime < workTimeInSeconds) {
            T object = generator.generate();
            queue.put(object);
            System.out.println("Producer add some resource - queue size is : " + queue.size());
        }
    }
}
