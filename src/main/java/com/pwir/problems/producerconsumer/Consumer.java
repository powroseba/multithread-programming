package com.pwir.problems.producerconsumer;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

class Consumer<T> implements Runnable {

    private final BlockingQueue<T> queue;
    private final long workTimeInSeconds;

    Consumer(BlockingQueue<T> queue, long workTimeInSeconds) {
        this.queue = queue;
        this.workTimeInSeconds = (workTimeInSeconds * 1000) + 1000;
    }

    @Override
    public void run() {
        try {
            consume();
        } catch (InterruptedException ex) {
            System.err.println("Consumer INTERRUPTED!");
        }
    }

    private void consume() throws InterruptedException {
        long startTime = System.currentTimeMillis();
        while(System.currentTimeMillis() - startTime < workTimeInSeconds) {
            take(this.queue);
            System.out.println("Consumer take some resource - queue size is : " + queue.size());
        }
    }

    private T take(BlockingQueue<T> queue) throws InterruptedException {
        return queue.poll(10, TimeUnit.SECONDS);
    }
}
