package com.pwir.problems;

import com.pwir.problems.extinguishing.FirefightersFactory;
import com.pwir.problems.peterson.PetersonFactory;
import com.pwir.problems.producerconsumer.ProducerConsumerFactory;
import com.pwir.problems.station.BusPassengerFactory;

import java.util.concurrent.BlockingQueue;

public class ProblemFactoryFacade {

    public Runnable buildProducer(BlockingQueue<String> queue) {
        return new ProducerConsumerFactory().buildProducer(queue);
    }

    public Runnable buildConsumer(BlockingQueue<String> queue) {
        return new ProducerConsumerFactory().buildConsumer(queue);
    }

    public Runnable buildPeterson(int numberOfThread) {
        return new PetersonFactory().buildPeterson(numberOfThread);
    }

    public Runnable[] buildFirefighters() {
        return new FirefightersFactory().buildFirefighters();
    }

    public Runnable buildBus(String busStopName) {
        return new BusPassengerFactory().buildBus(busStopName);
    }

    public Runnable buildPassenger(String busStopName) {
        return new BusPassengerFactory().buildPassenger(busStopName);
    }
}
