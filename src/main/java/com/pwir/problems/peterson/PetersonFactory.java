package com.pwir.problems.peterson;

import com.pwir.config.SystemProperties;

public class PetersonFactory {

    private static final String CRITICAL_OPERATION_TIME_TO_WAIT_PROPERTY = "peterson.critical.operation.wait.time_in_seconds";

    public Runnable buildPeterson(int numberOfThread) {
        return new Peterson(SystemProperties.getPropertyAsInteger(CRITICAL_OPERATION_TIME_TO_WAIT_PROPERTY), numberOfThread);
    }
}
