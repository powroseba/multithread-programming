package com.pwir.problems.peterson;

class Peterson implements Runnable {

    private static Boolean[] THREADS_FLAGS_TO_EXECUTE = { false, false };
    private static volatile int NEXT_THREAD_INDEX_TO_EXECUTE = -1;
    private final int timeToWait;
    private final int currentExecuteThreadIndex;

    Peterson(int timeToWait, int numberOfThread) {
        if (numberOfThread > 1) throw new IllegalArgumentException("Peterson algorithm support only two threads");
        this.timeToWait = timeToWait;
        this.currentExecuteThreadIndex = numberOfThread;
    }

    public void run() {
        THREADS_FLAGS_TO_EXECUTE[currentExecuteThreadIndex] = true;
        NEXT_THREAD_INDEX_TO_EXECUTE = nextTheadToExecuteIndex();
        while (isOtherThreadRun()) {
            lock();
        }
        try {
            doCriticalOperation();
        } catch (InterruptedException e) {
            THREADS_FLAGS_TO_EXECUTE[currentExecuteThreadIndex] = false;
        }
        THREADS_FLAGS_TO_EXECUTE[currentExecuteThreadIndex] = false;
    }

    private boolean isOtherThreadRun() {
        return THREADS_FLAGS_TO_EXECUTE[NEXT_THREAD_INDEX_TO_EXECUTE] && NEXT_THREAD_INDEX_TO_EXECUTE == nextTheadToExecuteIndex();
    }

    private int nextTheadToExecuteIndex() {
        return currentExecuteThreadIndex == 1 ? 0 : 1;
    }

    private void lock() {
        try {
            Thread.sleep(100);
            System.out.println("\t[ " + Thread.currentThread().getName() + " ] - Waiting ...");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doCriticalOperation() throws InterruptedException {
        System.out.println("[ " + Thread.currentThread().getName() + " ] - Starting critical operation");
        for (int i = 0; i < 10; i++) {
            Thread.sleep(timeToWait);
            System.out.println("\t[ " + Thread.currentThread().getName() + " ] - I am running!");
        }
    }

}