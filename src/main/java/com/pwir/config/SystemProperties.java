package com.pwir.config;

import java.util.Properties;

public class SystemProperties {

    private static final String THEAD_AMOUNT_PROPERTY = "threads.amount";
    private static final String DEFAULT_CONFIG_FILE_PATH = "config.properties";

    private static Properties systemProperties;

    private SystemProperties() {}

    public static void load(String configFilePath) {
        System.out.println("Loading system properties from file : " + configFilePath + "\n\n");
        systemProperties = new SystemPropertiesLoader(configFilePath).load();
    }

    public static void load() {
        System.out.println("Loading system properties from file : " + DEFAULT_CONFIG_FILE_PATH + "\n\n");
        systemProperties = new SystemPropertiesLoader(DEFAULT_CONFIG_FILE_PATH).load();
    }

    public static Integer getThreadsAmount() {
        return Integer.valueOf(getProperty(THEAD_AMOUNT_PROPERTY));
    }

    public static String getProperty(String name) {
        return systemProperties.getProperty(name);
    }

    public static Integer getPropertyAsInteger(String name) {
        return Integer.valueOf(systemProperties.getProperty(name));
    }

}
