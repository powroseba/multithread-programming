package com.pwir.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

class SystemPropertiesLoader {

    private final String configFilePath;

    SystemPropertiesLoader(String configFilePath) {
        this.configFilePath = configFilePath;
    }

    Properties load() {
        try (InputStream configInput = SystemPropertiesLoader.class.getClassLoader().getResourceAsStream(configFilePath)) {
            return loadPropertiesFromInput(configInput);
        } catch (IOException e) {
            System.err.println("Problem with configuration, config file not found!");
            e.printStackTrace();
            System.exit(0);
        }
        return new Properties();
    }

    private Properties loadPropertiesFromInput(InputStream input) throws IOException {
        Properties properties = new Properties();
        properties.load(input);
        return properties;
    }
}
