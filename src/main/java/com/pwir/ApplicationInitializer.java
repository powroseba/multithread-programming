package com.pwir;

import com.pwir.config.SystemProperties;
import com.pwir.executors.ProblemExecutorFacade;
import com.pwir.problems.ProblemFactoryFacade;

import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ApplicationInitializer {

    private static final Scanner SCANNER = new Scanner(System.in);

    public static void main(String[] args) {
        SystemProperties.load();
        ExecutorService threadPool = Executors.newFixedThreadPool(SystemProperties.getThreadsAmount());
        chooseProblemToExecute(threadPool);
        threadPool.shutdown();
    }

    private static void chooseProblemToExecute(ExecutorService threadPool) {
        ProblemExecutorFacade problemExecutorFacade = new ProblemExecutorFacade(threadPool, new ProblemFactoryFacade());
        printMenu();
        int option = SCANNER.nextInt();
        switch (option) {
            case 1: {
                problemExecutorFacade.executePetersonProblem();
                break;
            }
            case 2: {
                problemExecutorFacade.executeProducerConsumerProblem();
                break;
            }
            case 3: {
                problemExecutorFacade.executeFireFightingProblem();
                break;
            }
            case 4: {
                problemExecutorFacade.executeBusStationProblem();
                break;
            }
        }
    }

    private static void printMenu() {
        System.out.println("Execute problem : \n" +
                "[1] Execute Peterson problem.\n" +
                "[2] Execute producer-consumer problem.\n" +
                "[3] Execute Fire Fighting problem.\n" +
                "[4] Execute Bus Station problem.");
        System.out.print("\noption : ");
    }
}
