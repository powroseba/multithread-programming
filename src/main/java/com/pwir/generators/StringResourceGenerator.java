package com.pwir.generators;

import java.util.UUID;

public class StringResourceGenerator implements ResourceGenerator<String> {
    @Override
    public String generate() {
        try {
            Thread.sleep(250);
        } catch (InterruptedException e) {
            System.err.println("Generator generate INTERRUPTED!");
        }
        return UUID.randomUUID().toString();
    }
}
