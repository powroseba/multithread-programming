package com.pwir.generators;

public interface ResourceGenerator<T> {

    T generate();
}
